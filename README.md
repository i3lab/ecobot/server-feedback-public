# Ecobot server-feedback

This is a version of the [server](https://gitlab.com/i3lab/ecobot/server) adapted for the experimental study.

Please refer to the [README](https://gitlab.com/i3lab/ecobot/server/-/blob/master/README.md?ref_type=heads) of the main repo.
