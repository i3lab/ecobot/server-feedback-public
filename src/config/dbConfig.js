/**
 * Change this to connect your database to the application
 */
module.exports = {
  development: {
    username: "root",
    password: "root",
    database: "chatbot",
    host: "localhost",
    dialect: "mysql",
    logging: false,
  },
};
