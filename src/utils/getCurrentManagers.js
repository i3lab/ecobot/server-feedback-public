let currentChatbotManager = null;
let currentNotificationManager = null;

/**
 * This file is needed in order to avoid using global variables while calling APIs and support multi-session chatbot.
 */

exports.getCurrentChatbotManager = function () {
  return currentChatbotManager;
};

exports.setCurrentChatbotManager = function (newChatbotManager) {
  currentChatbotManager = newChatbotManager;
};

exports.getCurrentNotificationManager = function () {
  return currentNotificationManager;
};

exports.setCurrentNotificationManager = function (newNotificationManager) {
  currentNotificationManager = newNotificationManager;
};
