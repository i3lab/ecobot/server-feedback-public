module.exports = (app) => {
  const curiosities = require("../controllers/curiosityController");

  let router = require("express").Router();

  // Create a new Curiosity object
  router.post("/", curiosities.create);

  // Get all curiosities for an appliance
  router.get("/:applianceName", curiosities.findAllForAppliance);

  // Get all general curiosities
  router.get("/general/all", curiosities.findAllGeneral);

  app.use("/api/curiosities", router);
};
