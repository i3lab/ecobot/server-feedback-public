module.exports = (app) => {
  const consumptions = require("../controllers/consumptionController");

  let router = require("express").Router();

  // Create a new Consumption object (used to insert the mock data in the database)
  router.post("/", consumptions.create);

  // Update the Consumption with applianceName (used, for example, to set a goal or remove it)
  router.put("/:applianceName", consumptions.update);

  // Get all consumptions
  router.get("/", consumptions.findAll);

  // Get consumption by applianceName
  router.get("/:applianceName", consumptions.findOne);

  app.use("/api/consumptions", router);
};
