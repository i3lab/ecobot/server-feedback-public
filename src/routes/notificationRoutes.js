module.exports = (app) => {
  const notifications = require("../controllers/notificationController");

  let router = require("express").Router();

  // Add new notification
  router.post("/", notifications.create);

  // Get the notifications number
  router.get("/number", notifications.getNumber);

  app.use("/api/notifications", router);
};
