const { NlpManager } = require("node-nlp");
const intentsCreator = require("../utils/intentsCreator");
const entitiesCreator = require("../utils/entitiesCreator");
const slotsCreator = require("../utils/slotsCreator");
const ContextManager = require("./ContextManager");
const DialogManager = require("./DIalogManager");
const strings = require("../data/strings");
const NotificationManager = require("./NotificationManager");
const {
  setCurrentNotificationManager,
} = require("../utils/getCurrentManagers");
const saveSessionLog = require("../utils/saveSessionLog");

/**
 * This class is responsible for managing the NLP engine, and generating the answer to the user question,
 * including also Context information and Dialog management.
 */
class ChatbotManager {
  dialogManager;
  nlpManager;
  intents;
  contextManager;
  notificationManager;
  lastAnswer;
  constructor() {
    // initialise the various Managers (context, dialog, notification, ...) + NLP engine (slots, intents, entities)
    this.nlpManager = new NlpManager({
      languages: [strings.englishLanguage],
      forceNER: false,
    });
    this.intents = intentsCreator(this.nlpManager);
    entitiesCreator(this.nlpManager);
    slotsCreator(this.nlpManager);
    this.contextManager = new ContextManager();
    this.dialogManager = new DialogManager();
    this.notificationManager = new NotificationManager();
    setCurrentNotificationManager(this.notificationManager); // necessary to access the notification manager from the API

    this.lastAnswer = null; // used by the API to retrieve the answer to the last question made by the user

    this.trainManager(); // generates the NLP model (model.nlp)
  }

  // this function is used to keep memory of the conversation flow, for example by updating the current and previous intent
  updateConversationStatus(nlpAnswer, updateCurrentIntent) {
    this.dialogManager.specifyApplianceState = nlpAnswer; // necessary to know if the user is trying to specify an appliance name, or not

    if (updateCurrentIntent && nlpAnswer.intent !== strings.noneIntent) {
      // sometimes, as in the SpecifyApplianceIntent case, we don't want to update the current intent because it's not user init but chatbot init
      // moreover, if the user says something that is not recognized, we don't want to update the current intent
      this.dialogManager.currentIntent = nlpAnswer.intent;
    }
  }

  // this function is used to answer to a user input. It can be triggered, for example, by API call.
  async answerToUser(input, updateCurrentIntent = true) {
    saveSessionLog("User > " + input + "\n");
    // as first thing, include context in the user input. For example, if the user says 'set a goal' and the context
    // contains appliance: 'washing machine', the new input will be 'set a goal for washing machine'.
    const inputWithContext = this.contextManager.includeContext(
      input,
      this.nlpManager
    );

    // then, process the input with the NLP engine to obtain the NLP answer (JSON with a lot of useful information)
    const nlpAnswer = await this.nlpManager.process(
      strings.englishLanguage,
      await inputWithContext,
      null,
      null
    );

    if (nlpAnswer.answer) {
      // print the answer of the chatbot. This method saves also the answer in the variable global.lastAnswer, used by
      // the APIs to show the answer to the user in the frontend.
      this.printChatbotAnswer(nlpAnswer.answer);
    }

    if (strings.printLogs) {
      this.printChatbotAnswer(JSON.stringify(nlpAnswer));
    }

    // update the conversation status (current and previous intent)
    this.updateConversationStatus(nlpAnswer, updateCurrentIntent);

    if (strings.printLogs) {
      console.log(
        "           LOG: previousIntent: " + this.dialogManager.previousIntent
      );
    }

    // execute the intent action. In fact, an intent not only has an 'answer', but could also have an associated action.
    // for example, the 'SetGoalIntent' has an action that sets the goal in the database. The correct action is found by
    // scanning the intents array, and executing intentAction() if the intent name is the same of the intent recognized by the NLP engine.
    await this.intents
      .find((i) => i.intentName === nlpAnswer.intent)
      ?.intentAction(
        nlpAnswer,
        this.dialogManager,
        this.contextManager,
        this.intents,
        this,
        this.notificationManager
      );

    return nlpAnswer;
  }

  // This function is not frontend dependent, since the answer is contained in the lastAnswer parameter and can be set and retrieved with the API
  printChatbotAnswer(answer) {
    console.log("Bot  > " + answer); // can be removed if the logs are not necessary, since the answer will be in lastAnswer parameter.
    // the frontend can simply retrieve the answer to a question thanks to the API (see src/routes/answerRoutes.js),
    // that execute the answerToUser function and saved the answer in lastAnswer variable, returning it in the response file
    saveSessionLog("Bot  > " + answer + "\n");
    this.lastAnswer = answer;
  }

  // when the app is launched, trains the manager to generate the NLP model (model.nlp)
  trainManager() {
    this.nlpManager.train().then(() => {
      const initialMessage = {
        en: "Hello! I'm here to help. Ask me something.",
        it: "Ciao! Sono qui per aiutarti. Chiedimi qualcosa.",
      };
      saveSessionLog(
        "\n-------- NEW SESSION STARTED AT " + new Date() + " --------\n"
      );
      this.printChatbotAnswer(initialMessage[strings.currentLanguage]);
    });
  }
}

module.exports = ChatbotManager;
