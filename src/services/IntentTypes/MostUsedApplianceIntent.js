const Intent = require("./Intent");
const strings = require("../../data/strings");
const { loadAllConsumptions } = require("../../utils/makeAPICall");

/**
 * This is the intent will list the most used, and least used, appliances
 */

const utterances = {
  en: [],
  it: [
    "quale elettrodomestico ho usato di piu",
    "quale elettrodomestico ho usato di meno",
    "quale apparecchio ho usato di piu",
    "quale apparecchio ho usato di meno",
    "cosa ho usato di piu",
    "cosa ho usato di meno",
    "quale elettrodomestico consuma di piu",
    "quale elettrodomestico consuma di meno",
    "quali sono gli elettrodomestici che consumano di piu",
    "quali sono gli elettrodomestici che consumano di meno",
    "che cosa consuma di piu",
    "che cosa consuma di meno",
    "che cosa consuma piu energia",
    "che cosa consuma meno energia",
    "cosa sta consumando energia",
    "cosa sta consumando di piu",
    "cosa sta consumando di meno",
    "che cosa sta consumando piu energia",
    "che cosa sta consumando meno energia",
    "come mai sto consumando cosi tanta energia",
    "cosa ho usato di piu oggi",
    "cosa ho usato di meno oggi",
    "come mai sto consumando energia",
    "come mai consumo cosi tanto",
    "quali elettrodomestici consumano di più",
    "quali elettrodomestici consumano di meno",
    "quali dispositivi consumano di più",
    "quali dispositivi consumano di meno",
    "quali elettrodomestici usano piu energia",
    "quali elettrodomestici usano meno energia",
    "quali sono gli elettrodomestici che consumano di più in casa mia",
    "quali sono i dispositivi che consumano di più in casa mia",
    "quali elettrodomestici dovrei usare di meno?",
    "quali elettrodomestici dovrei utilizzare meno",
    "elettrodomestici che dovrei usare meno",
    "che elettrodomestico consuma di piu?",
    "quali elettrodomestici stanno consumando maggiormente",
    "quali dispositivi stanno consumando maggiormente",
    "qual è il consumo maggiore",
  ],
};

class MostUsedApplianceIntent extends Intent {
  constructor(nlpManager) {
    super(
      strings.mostUsedApplianceIntent,
      utterances[strings.currentLanguage],
      [],
      nlpManager
    );
  }

  async intentAction(
    nlpAnswer,
    dialogManager,
    contextManager,
    intents,
    chatbotManager
  ) {
    const consumptionData = await loadAllConsumptions();

    // sort consumptions ordered by averageUserConsumptions
    let sortedConsumptions = consumptionData.data.sort((a, b) =>
      a.averageUserConsumptions > b.averageUserConsumptions ? -1 : 1
    );

    // remove entire house from the consumptions list
    sortedConsumptions = sortedConsumptions.filter(
      (consumption) =>
        consumption.applianceName !== strings.entirehouseAppliance.id
    );

    // get the first 3 appliances
    const top3Appliances = sortedConsumptions.slice(0, 3);

    // get the last 3 appliances
    const last3Appliances = sortedConsumptions.slice(-3).reverse();

    // print the first 3 appliances with the consumptions, and the last 3 appliances with the consumptions
    const answer = {
      it:
        "Gli elettrodomestici che hanno consumato di più sono: " +
        top3Appliances.map(
          (appliance) =>
            " " +
            strings[
              appliance.applianceName.toLowerCase().replace(/\s/g, "") +
                "Appliance"
            ][strings.currentLanguage][0] +
            " con " +
            appliance.averageUserConsumptions +
            " kWh"
        ) +
        ". Invece, gli elettrodomestici che hanno consumato di meno, sono: " +
        last3Appliances.map(
          (appliance) =>
            " " +
            strings[
              appliance.applianceName.toLowerCase().replace(/\s/g, "") +
                "Appliance"
            ][strings.currentLanguage][0] +
            " con " +
            appliance.averageUserConsumptions +
            " kWh"
        ) +
        ". Se vuoi avere un suggerimento su come consumare meno energia, scrivi 'dammi suggerimento per " +
        strings[
          top3Appliances[0].applianceName.toLowerCase().replace(/\s/g, "") +
            "Appliance"
        ][strings.currentLanguage][0] +
        "'",
    };
    chatbotManager.printChatbotAnswer(answer[strings.currentLanguage]);
  }
}

module.exports = MostUsedApplianceIntent;
