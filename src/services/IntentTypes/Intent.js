const strings = require("../../data/strings");

/**
 * Interface class for all the intents. It cannot be triggered by the user, but it is used
 * to create new intents. It contains the basic information about the intent in the constructor, ù
 * such as the intent name, the possible utterances and the possible answers. In this way, creating
 * a child of this class is possible to create a new intent. When done, please add the "new intent..."
 * keyword in the intentsCreator.js file.
 *
 * It's possible to specify the action to be performed when the intent is triggered by the user by extending
 * the intentAction() method. This method will be triggered by the ChatbotManager when the intent is recognized.
 */
class Intent {
  constructor(intentName, utterances, answers, nlpManager) {
    this._intentName = intentName;
    this._utterances = utterances; // necessary since through the manager is not possible to access the utterances of an intent
    utterances.forEach((utterance) =>
      nlpManager.addDocument(
        strings.englishLanguage,
        utterance,
        this._intentName
      )
    );
    answers.forEach((answer) =>
      nlpManager.addAnswer(
        strings.englishLanguage,
        this._intentName,
        answer,
        null
      )
    );
  }

  get intentName() {
    return this._intentName;
  }

  get utterances() {
    return this._utterances;
  }

  intentAction() {}
}

module.exports = Intent;
