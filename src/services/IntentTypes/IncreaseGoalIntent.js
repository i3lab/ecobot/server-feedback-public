const Intent = require("./Intent");
const strings = require("../../data/strings");
const {
  loadConsumptionData,
  updateConsumption,
} = require("../../utils/makeAPICall");
const ContextManager = require("../ContextManager");

/**
 * This intent allows the user to increase the difficulty of the goal of a specific appliance.
 * It is usually triggered after the EditGoalIntent.
 */

const utterances = {
  en: [
    "increase goal for @appliance",
    "increase for @appliance",
    "increase @appliance goal",
    "goal for @appliance is too easy",
    "easy for @appliance",
    "too easy for @appliance",
    "too low for @appliance",
    "goal for @appliance is too low",
  ],
  it: [
    "aumenta la difficoltà dell'obiettivo per @appliance",
    "aumenta la difficoltà del goal per @appliance",
    "aumenta @appliance",
    "facile per @appliance",
    "il goal per @appliance è troppo facile",
    "troppo facile per @appliance",
    "aumenta @appliance",
    "aumenta la difficoltà dell'obiettivo per @appliance",
    "aumentane la difficoltà @appliance",
    "l'obiettivo per @appliance è troppo facile da raggiungere",
  ],
};

class IncreaseGoalIntent extends Intent {
  constructor(nlpManager) {
    super(
      strings.increaseGoalIntent,
      utterances[strings.currentLanguage],
      [],
      nlpManager
    );
  }

  async intentAction(
    nlpAnswer,
    dialogManager,
    contextManager,
    intents,
    chatbotManager
  ) {
    // extract from the nlpAnswer the appliance name
    let currentAppliance;
    let currentApplianceText;
    for (const entity of nlpAnswer.entities) {
      if (entity.entity === strings.applianceEntity) {
        currentAppliance = entity.option;
        currentApplianceText = entity.sourceText;
      }
    }
    // if the appliance name was specified, we can continue. Otherwise, the slotFilling will ask the user to specify the appliance name, and we can close the intentAction
    if (!currentAppliance) {
      return;
    }

    // we can assume that the db is already filled with the list of all appliance in the appliances table
    const previousConsumptionsData = await loadConsumptionData(
      currentAppliance
    );

    // check if a goal is already set for the appliance
    if (!previousConsumptionsData.data.goal) {
      let generatedAnswer;
      if (strings.currentLanguage === "en") {
        generatedAnswer =
          "You still don't have a goal set for " +
          currentApplianceText +
          ". Do you want to set a goal for it now? ";
      } else if (strings.currentLanguage === "it") {
        generatedAnswer =
          "Non hai ancora impostato un obiettivo per " +
          currentApplianceText +
          ". Vuoi impostarlo adesso? ";
      }
      // for that appliance, there is no goal set.
      chatbotManager.printChatbotAnswer(generatedAnswer);

      // put in the context the set goal intent, in order to set a goal if the users answers yes in the yesNoIntent
      ContextManager.prototype.continuePreviousIntent = async function () {
        let generatedAnswer;
        if (strings.currentLanguage === "en") {
          generatedAnswer = "set a goal for " + currentApplianceText;
        } else if (strings.currentLanguage === "it") {
          generatedAnswer = generatedAnswer =
            "imposta un goal per " + currentApplianceText;
        }
        await chatbotManager
          .answerToUser(generatedAnswer, false)
          .then(() => {});
      };

      // set the continuePreviousIntent action as valid (for 1 intent)
      contextManager.continuePreviousIntentTokens = 2;
    } else {
      await updateConsumption(
        currentAppliance,
        previousConsumptionsData.data.currentConsumptions,
        previousConsumptionsData.data.averageGlobalConsumptions,
        previousConsumptionsData.data.averageUserConsumptions,
        Math.round(previousConsumptionsData.data.goal * 0.9), // the goal is 10% more difficult now
        previousConsumptionsData.data.goalPeriodEnd
      );
      let generatedAnswer;
      if (strings.currentLanguage === "en") {
        generatedAnswer =
          "Okay! I'll increase by 10% the difficulty of the goal. Now, you have to save " +
          Math.round(previousConsumptionsData.data.goal * 0.9) +
          " kWh instead of " +
          previousConsumptionsData.data.goal +
          " kWh for " +
          currentApplianceText +
          "!";
      } else if (strings.currentLanguage === "it") {
        generatedAnswer =
          "Ok! Aumento del 10% la difficoltà dell'obiettivo. Ora, dovrai risparmiare " +
          Math.round(previousConsumptionsData.data.goal * 0.9) +
          " kWh invece di " +
          previousConsumptionsData.data.goal +
          " kWh per " +
          currentApplianceText +
          "!";
      }
      chatbotManager.printChatbotAnswer(generatedAnswer);
    }
  }
}

module.exports = IncreaseGoalIntent;
