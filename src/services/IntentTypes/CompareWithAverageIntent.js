const Intent = require("./Intent");
const strings = require("../../data/strings");
const { loadConsumptionData } = require("../../utils/makeAPICall");

/**
 * This intent will compare user consumptions with the average for other users.
 */

const utterances = {
  en: ["compare my consumptions with the average @appliance"],
  it: [
    "sto consumando di più rispetto alla media per @appliance",
    "sto consumando di piu o di meno rispetto alla media per @appliance",
    "consumi medi per @appliance",
    "consumi degli altri per @appliance",
    "sto consumando troppo per @appliance?",
    "i miei consumi sono troppi per @appliance",
    "consumi medi degli altri per @appliance",
    "quanto consumano le persone in media per @appliance",
    "quanto consumano le persone per @appliance",
    "quanto consumano gli altri utenti per @appliance",
    "consumi medi di @appliance",
    "sto utilizzando troppo @appliance?",
    "dimmi se sto consumando troppo per @appliance",
    "dimmi se sto utilizzando troppo @appliance",
    "consumo troppo per @appliance",
    "utilizzo troppo @appliance",
    "in media quanto consuma @appliance",
    "sto consumando di meno rispetto alla media per @appliance",
    "sto consumando di meno rispetto agli altri per @appliance",
    "sto consumando di piu rispetto agli altri per @appliance",
    "consumo di piu rispetto agli altri per @appliance",
    "sono meglio degli altri per @appliance",
    "consumo di piu degli altri per @appliance",
    "sto consumando eccessivamente per @appliance",
    "confronta i miei consumi con la media per @appliance",
    "confronto dei miei consumi con la media per @appliance",
    "confronta i miei consumi con quelli degli altri per @appliance",
    "sono piu bravo degli altri per @appliance",
    "sono piu brava degli altri per @appliance",
    "sono meglio degli altri per @appliance",
    "sono migliore degli altri per @appliance",
    "sono bravo a risparmiare per @appliance?",
    "sono brava a risparmiare per @appliance?",
    "sono bravo con i risparmi per @appliance?",
    "sono brava con i risparmi per @appliance?",
    "quanto consumano le altre persone per la @televisione",
    "confrontare i miei consumi energetici con quelli di altri utenti per @televisione",
    "come posso confrontare i miei consumi energetici con altri utenti @appliance",
    "confronta i miei consumi @appliance con quelli degli altri",
    "puoi dirmi se consumo di piu rispetto agli altri per @appliance",
  ],
};

class CompareWithAverageIntent extends Intent {
  constructor(nlpManager) {
    super(
      strings.compareWithAverageIntent,
      utterances[strings.currentLanguage],
      [],
      nlpManager
    );
  }

  async intentAction(
    nlpAnswer,
    dialogManager,
    contextManager,
    intents,
    chatbotManager
  ) {
    // extract from the nlpAnswer the appliance name
    let currentAppliance;
    let currentApplianceText;
    for (const entity of nlpAnswer.entities) {
      if (entity.entity === strings.applianceEntity) {
        currentAppliance = entity.option;
        currentApplianceText = entity.sourceText;
      }
    }

    // if the appliance name was specified, we can continue. Otherwise, the slotFilling will ask the user to specify the appliance name, and we can close the intentAction
    if (!currentAppliance) {
      return;
    }

    // we can assume that the db is already filled with the list of all appliance in the appliances table
    const consumptionsData = await loadConsumptionData(currentAppliance);

    if (!consumptionsData.data.averageUserConsumptions) {
      // we don't have enough data about the user to compare with the average
      const generatedAnswer = {
        en:
          "I'm sorry, I don't have enough data about your " +
          currentApplianceText +
          " average consumptions to make a comparison with the other users average.",
        it:
          "Mi dispiace, non ho abbastanza dati sui tuoi utilizzi di " +
          currentApplianceText +
          " per fare un confronto con la media degli altri utenti.",
      };

      chatbotManager.printChatbotAnswer(
        generatedAnswer[strings.currentLanguage]
      );
    } else {
      // we have enough data
      // we print 'more' or 'less' depending on the comparison between the user consumptions and the average
      const comparisonWord = {
        en:
          consumptionsData.data.averageUserConsumptions >
          consumptionsData.data.averageGlobalConsumptions
            ? "more"
            : "less",
        it:
          consumptionsData.data.averageUserConsumptions >
          consumptionsData.data.averageGlobalConsumptions
            ? "di più"
            : "di meno",
      };
      const generatedAnswer = {
        en:
          "You are consuming " +
          comparisonWord.en +
          " than the average for " +
          currentApplianceText +
          ". In fact, " +
          consumptionsData.data.averageUserConsumptions +
          " kWh is your average consumption, while the average for the other users is " +
          consumptionsData.data.averageGlobalConsumptions +
          "kWh.",
        it:
          "Di solito, consumi " +
          comparisonWord.it +
          " rispetto alla media degli altri utenti per " +
          currentApplianceText +
          ". Infatti, " +
          consumptionsData.data.averageUserConsumptions +
          " kWh è il tuo consumo medio in un mese, mentre la media per gli altri utenti è " +
          consumptionsData.data.averageGlobalConsumptions +
          "kWh.",
      };

      chatbotManager.printChatbotAnswer(
        generatedAnswer[strings.currentLanguage]
      );
    }
  }
}

module.exports = CompareWithAverageIntent;
