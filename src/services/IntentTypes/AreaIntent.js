const Intent = require("./Intent");
const strings = require("../../data/strings");
const {
  loadUserData,
  loadAreaData,
  loadAllConsumptions,
} = require("../../utils/makeAPICall");
const ContextManager = require("../ContextManager");

/**
 * This intent allows the user to check if their consumptions are better or worse than
 * the average in their area. If the user area is not known, the chatbot will ask for it.
 */

const utterances = {
  en: [
    "what is my position in the area goal chart?",
    "what are average consumptions in my area?",
    "am i consuming more or less of my area?",
    "am i better than the average consumption of my area?",
    "am i consuming more kwh than the average consumption of my area?",
  ],
  it: [
    "qual è posizione nella classifica delle aree?",
    "qual è posizione nella classifica delle regioni?",
    "qual è posizione nella classifica dei goal?",
    "quali sono i consumi medi della area?",
    "quali sono i consumi medi della regione?",
    "quanto consuma area?",
    "quanto consuma regione?",
    "consumo di più o di meno della area?",
    "consumo di più o di meno della regione?",
    "sono meglio della media dei consumi della area?",
    "sono meglio della media dei consumi della regione?",
    "sto consumando di più o di meno della area?",
    "sto consumando di più o di meno della regione?",
    "sto consumando più kwh della media dei consumi della area?",
    "sto consumando più kwh della media dei consumi della regione?",
    "confronta i consumi con quelli della area",
    "confronta i consumi con quelli della regione",
    "confronto con regione",
    "confronto con area",
    "puoi dirmi se consumo di piu rispetto area",
    "puoi dirmi se consumo di meno rispetto area",
    "puoi dirmi se consumo di piu rispetto regione",
    "puoi dirmi se consumo di meno rispetto regione",
    "puoi dirmi quanto consumo rispetto area",
    "puoi dirmi quanto consuma regione",
  ],
};

class AreaIntent extends Intent {
  constructor(nlpManager) {
    super(
      strings.areaIntent,
      utterances[strings.currentLanguage],
      [],
      nlpManager
    );
  }

  // eslint-disable-next-line no-unused-vars
  async intentAction(
    nlpAnswer,
    dialogManager,
    contextManager,
    intents,
    chatbotManager
  ) {
    // Load user data (ie tha area) from the database
    const userData = await loadUserData();

    // If the user area is not known, ask for it
    if (!userData || !userData.data.region) {
      let generatedAnswer;
      if (strings.currentLanguage === "en") {
        generatedAnswer =
          "In which italian region do you live? I'll remember this in the future! ";
      } else if (strings.currentLanguage === "it") {
        generatedAnswer =
          "In quale regione italiana abiti? Me lo ricorderò in futuro! ";
      }
      chatbotManager.printChatbotAnswer(generatedAnswer);
      return;
    }

    // If the user area is known, the average data for their area is loaded from the database, as well as the user consumptions
    const areaData = await loadAreaData(userData.data.region);
    const areaAverageAreaConsumptions = areaData.data.averageConsumptions;
    let totalAverageUserConsumption = 0;

    const userConsumptionsData = await loadAllConsumptions();

    if (userConsumptionsData) {
      userConsumptionsData.data.forEach((consumption) => {
        if (consumption.applianceName !== strings.entirehouseAppliance.id) {
          totalAverageUserConsumption += consumption.averageUserConsumptions;
        }
      });
    }

    if (totalAverageUserConsumption < areaAverageAreaConsumptions) {
      let generatedAnswer;
      if (strings.currentLanguage === "en") {
        generatedAnswer =
          "You are consuming, on average, " +
          (areaAverageAreaConsumptions - totalAverageUserConsumption) +
          " kWh less than the average in " +
          userData.data.region +
          "! Keep it up!";
      } else if (strings.currentLanguage === "it") {
        generatedAnswer =
          "Stai consumando, in media, " +
          (areaAverageAreaConsumptions - totalAverageUserConsumption) +
          " kWh in meno rispetto alla media in " +
          userData.data.region +
          "! Continua così!";
      }
      chatbotManager.printChatbotAnswer(generatedAnswer);
    } else {
      let generatedAnswer;
      if (strings.currentLanguage === "en") {
        generatedAnswer =
          "You are consuming, on average, " +
          (totalAverageUserConsumption - areaAverageAreaConsumptions) +
          " kWh more than the average in " +
          userData.data.region +
          "! Do you want to set a goal for the entire house to try to consume less energy?";
      } else if (strings.currentLanguage === "it") {
        generatedAnswer =
          "Stai consumando, in media, " +
          (totalAverageUserConsumption - areaAverageAreaConsumptions) +
          " kWh in più rispetto alla media in " +
          userData.data.region +
          "! Vuoi impostare un goal per tutta la casa per provare a consumare meno energia?";
      }
      chatbotManager.printChatbotAnswer(generatedAnswer);
    }

    // Include in the context the goal.set intent action, that will be executed by the
    // YesNoIntent just if the user agrees (i.e.: if the user answers 'yes'). So, if the user answers 'yes', a goal will
    // be set for the entire house with as goal the average consumption of the area.

    ContextManager.prototype.continuePreviousIntent = async function () {
      let generatedAnswer;
      if (strings.currentLanguage === "en") {
        generatedAnswer = "set a goal for entire house";
      } else if (strings.currentLanguage === "it") {
        generatedAnswer = "imposta un obiettivo per la casa intera";
      }
      await chatbotManager.answerToUser(generatedAnswer, false).then(() => {});
    };

    // set the continuePreviousIntent action as valid (for 1 intent)
    contextManager.continuePreviousIntentTokens = 2;
  }
}

module.exports = AreaIntent;
