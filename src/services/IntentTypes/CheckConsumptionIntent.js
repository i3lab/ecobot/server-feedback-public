const Intent = require("./Intent");
const strings = require("../../data/strings");
const { loadConsumptionData } = require("../../utils/makeAPICall");

/**
 * This intent allows the user to check their consumptions. In general, or for a specific appliance.
 */

const utterances = {
  en: [
    "what is my consumption for @appliance?",
    "what are my consumptions for @appliance?",
    "Current consumption for @appliance",
    "How much do I consume for @appliance?",
    "How much I'm consuming this month for @appliance?",
    "Consumptions for @appliance",
    "Monthly consumptions for @appliance",
    "How many kWh I'm consuming for @appliance?",
  ],
  it: [
    "qual è il mio consumo per @appliance?",
    "quali sono i miei consumi per @appliance?",
    "consumo attuale per @appliance",
    "quanto consumo per @appliance?",
    "quanto sto consumando questo mese per @appliance?",
    "consumi per @appliance",
    "consumi di @appliance",
    "consumi del mese di @appliance",
    "quanto ho consumato per @appliance?",
    "quanto ho consumato nell'ultimo periodo per @appliance?",
    "consumi del mese per @appliance",
    "quanti kWh sto consumando per @appliance?",
    "dimmi quanto sto consumando per @appliance",
    "quanto consuma @appliance",
    "quali sono i consumi di @appliance",
    "e il suo consumo in kwh?",
    "quanto sto consumando?",
    "quali sono i miei consumi",
    "e quanto sto consumando per @appliance?",
    "quanto consumano @appliance",
    "quanto consumano gli @appliance insieme",
    "a quanto ammonta il consumo di @appliance",
    "puoi dirmi i consumi di @appliance",
    "puoi dirmi i consumi di @appliance per questo mese",
    "puoi dirmi i consumi di @appliance per l'ultimo periodo",
    "puoi dirmi quanto consuma @appliance",
    "puoi dirmi quanto ho consumato con @appliance",
    "monitora @appliance",
    "monitora i consumi di @appliance",
    "monitora i consumi di @appliance per questo mese",
    "monitora i consumi di @appliance per l'ultimo periodo",
    "quanto consuma la mia @appliance",
  ],
};

class CheckConsumptionIntent extends Intent {
  constructor(nlpManager) {
    super(
      strings.checkConsumptionIntent,
      utterances[strings.currentLanguage],
      [],
      nlpManager
    );
  }

  async intentAction(
    nlpAnswer,
    dialogManager,
    contextManager,
    intents,
    chatbotManager
  ) {
    // extract from the nlpAnswer the appliance name
    let currentAppliance;
    let currentApplianceText;
    for (const entity of nlpAnswer.entities) {
      if (entity.entity === strings.applianceEntity) {
        currentAppliance = entity.option;
        currentApplianceText = entity.sourceText;
      }
    }

    // if the appliance name was specified, we can continue. Otherwise, the slotFilling will ask the user to specify the appliance name, and we can close the intentAction
    if (!currentAppliance) {
      return;
    }

    // we can assume that the db is already filled with the list of all appliance in the appliances table
    const previousConsumptionsData = await loadConsumptionData(
      currentAppliance
    );

    // check if there are current (or previous) consumptions for that appliance
    let finalAnswer = "";
    if (previousConsumptionsData.data.currentConsumptions) {
      // during this month the user has already used that appliance. We can find the consumptions for the current month
      if (strings.currentLanguage === "en") {
        finalAnswer +=
          "This month, you have already consumed " +
          Math.round(previousConsumptionsData.data.currentConsumptions) +
          "kWh for " +
          currentApplianceText +
          ".";
      } else if (strings.currentLanguage === "it") {
        finalAnswer +=
          "Questo mese, hai già consumato " +
          Math.round(previousConsumptionsData.data.currentConsumptions) +
          "kWh per " +
          currentApplianceText +
          ".";
      }
    }

    if (previousConsumptionsData.data.averageUserConsumptions) {
      // we have average data for the user consumptions for the past months. We can find the consumptions for the average consumptions
      if (finalAnswer !== "") {
        finalAnswer += " "; // to put a space between the two sentences
      }
      if (strings.currentLanguage === "en") {
        finalAnswer +=
          "On average, during the last period, you have consumed " +
          Math.round(previousConsumptionsData.data.averageUserConsumptions) +
          "kWh for " +
          currentApplianceText +
          ".";
      } else if (strings.currentLanguage === "it") {
        finalAnswer +=
          "In media, invece, consumi " +
          Math.round(previousConsumptionsData.data.averageUserConsumptions) +
          "kWh al mese per " +
          currentApplianceText +
          ".";
      }
    }

    if (finalAnswer === "") {
      // we don't have any data for the user.
      if (strings.currentLanguage === "en") {
        finalAnswer =
          "During this month and in the past, you haven't used " +
          currentAppliance +
          ".";
      } else if (strings.currentLanguage === "it") {
        finalAnswer =
          "Questo mese e in passato, non hai usato " +
          currentApplianceText +
          ".";
      }
    }

    chatbotManager.printChatbotAnswer(finalAnswer);
  }
}

module.exports = CheckConsumptionIntent;
