const Intent = require("./Intent");
const strings = require("../../data/strings");

/**
 * This intent can be used by the user to know the functionalities of the chatbot.
 */

const utterances = {
  en: [
    "what can i do?",
    "help",
    "give me help",
    "help me",
    "what can you do?",
    "tutorial",
    "hello",
    "hi",
  ],
  it: [
    "cosa posso chiederti?",
    "aiuto",
    "aiutami",
    "cosa puoi fare?",
    "tutorial",
    "ciao",
    "come mi puoi aiutare?",
    "a cosa servi?",
    "a che cosa servi",
    "come fai",
    "come ti chiami",
    "qual è il tuo nome",
    "come ti posso chiamare",
    "cosa posso fare",
    "che cosa posso fare?",
  ],
};

const answers = {
  en: [
    "Hello! My name is EcoBot, and I will help you to save energy each month. You can set a goal for a specific appliance (eg: try to say 'set a goal for my television', receive a suggestion to save energy ('give me a suggestion for television' or check if you are consuming more or less than your area ('Am I consuming more or less of my area?'). Try to type something!",
  ],
  it: [
    "Ciao! Mi chiamo EcoBot, e ti aiuterò a risparmiare energia. Ti posso dare suggerimenti e curiosità (prova a dire 'dammi una curiosità per la televisione' o 'dammi un suggerimento per il condizionatore'), e monitorare i tuoi consumi (prova con 'quali elettrodomestici dovrei usare di meno?' o 'quanto spendo per la caldaia ogni mese?').",
  ],
};

class HelpIntent extends Intent {
  constructor(nlpManager) {
    super(
      strings.helpIntent,
      utterances[strings.currentLanguage],
      answers[strings.currentLanguage],
      nlpManager
    );
  }

  intentAction() {}
}

module.exports = HelpIntent;
