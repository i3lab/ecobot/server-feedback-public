const Intent = require("./Intent");
const strings = require("../../data/strings");

/**
 * This is the intent triggered when the user wants to manage a device, to tell them that is not capable of doing that.
 */

const utterances = {
  en: [],
  it: [
    "accendi",
    "spegni",
    "accendi @appliance",
    "spegni @appliance",
    "riduci la potenza di @appliance",
    "riduci la potenza del @appliance",
    "diminuisci la potenza di @appliance",
    "diminuisci la potenza del @appliance",
    "cambia modalità @appliance",
    "imposta la modalità eco",
    "attiva la modalità eco",
    "metti la modalità eco",
    "compra una nuova @appliance",
    "compra un nuovo @appliance",
    "stacca le spine",
    "stacca la spina di @appliance",
    "chiudi le finestre",
    "togli la modalità intensiva",
    "togli il programma cotone",
    "puoi spegnere @appliance",
    "puoi accendere @appliance",
    "puoi spegnere il @appliance",
    "puoi accendere il @appliance",
    "puoi togliere la modalità eco",
    "spegnere @appliance",
  ],
};

const answers = {
  en: ["IMPLEMENT"],
  it: [
    "Mi dispiace, vorrei poterti aiutare ma purtroppo non posso accendere, spegnere o controllare lo stato di un elettrodomestico direttamente. Puoi però chiedermi tante altre cose: prova a scrivere 'aiutami' per avere più informazioni.",
    "Mi dispiace, vorrei poterti aiutare ma purtroppo non posso accendere, spegnere o controllare lo stato di un elettrodomestico direttamente. Posso solo monitorarli nel tempo per mostrarti i tuoi consumi e darti suggerimenti.",
  ],
};
class ManageDeviceIntent extends Intent {
  constructor(nlpManager) {
    super(
      strings.manageDeviceIntent,
      utterances[strings.currentLanguage],
      answers[strings.currentLanguage],
      nlpManager
    );
  }

  intentAction() {}
}

module.exports = ManageDeviceIntent;
