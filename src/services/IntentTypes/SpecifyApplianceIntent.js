const Intent = require("./Intent");
const strings = require("../../data/strings");

/**
 * This intent allows the user to specify the appliance they are referring to in the previous intent.
 * It uses the dialogManager to know the previous intent. For example, if the user asks 'set a goal',
 * through the slot filling of nlp.js the chatbot will answer 'for which appliance?'. The user will
 * answer with the name of the appliance, triggering this intent. The chatbot will then know that the
 * user wants to set a goal (since the dialogManager.previousIntent = goal.set) for that appliance.
 */
class SpecifyApplianceIntent extends Intent {
  constructor(nlpManager) {
    super(strings.specifyApplianceIntent, ["@appliance"], [], nlpManager);
  }

  async intentAction(
    nlpAnswer,
    dialogManager,
    contextManager,
    intents,
    chatbotManager
  ) {
    const previousIntent = dialogManager.previousIntent; // will be triggered after this intent
    // specifyApplianceState: this information is used in order to trigger the None intent if the user specify
    // an appliance name out of the blue, when it's not requested from the chatbot
    if (dialogManager.specifyApplianceState) {
      const utterance = intents.find((i) => i.intentName === previousIntent)
        ?.utterances[0]; // get the first utterance of the previous intent
      const applianceWithAt = "@" + strings.applianceEntity; // @appliance of the utterance of the previous intent will be substituted with the appliance name
      let currentApplianceText;
      for (const entity of nlpAnswer.entities) {
        if (entity.entity === strings.applianceEntity) {
          currentApplianceText = entity.sourceText;
        }
      }
      await chatbotManager
        .answerToUser(
          utterance.replace(applianceWithAt, currentApplianceText), // as if the user had said the utterance with the appliance name
          false
        )
        .then(() => {});
    } else {
      await chatbotManager.answerToUser("", false).then(() => {}); // fires a none intent. In fact, we are not in an specifyApplianceState
    }
  }
}

module.exports = SpecifyApplianceIntent;
