const Intent = require("./Intent");
const strings = require("../../data/strings");

/**
 * This intent allows the user to confirm, or not, a previous request made by the chatbot (i.e.: a 'Notification').
 * The action to be performed if the user answers yes was previously saved in the ContextManager by the previous intent.
 * If a 'positive' answer is recognised, that action will be executed, otherwise, no.
 */

const utterances = {
  en: ["yes", "okay", "fine", "ok", "yeah", "yep", "sure", "no", "nope"],
  it: [
    "sì",
    "ok",
    "okay",
    "va bene",
    "si",
    "certo",
    "si certo",
    "si ok",
    "si va bene",
    "no",
    "nope",
    "sisi",
  ],
};

class YesNoIntent extends Intent {
  constructor(nlpManager) {
    super(
      strings.yesNoIntent,
      utterances[strings.currentLanguage],
      [],
      nlpManager
    );
  }

  async intentAction(
    nlpAnswer,
    dialogManager,
    contextManager,
    intents,
    chatbotManager
  ) {
    if (contextManager.continuePreviousIntentTokens === 0) {
      // fires a none intent. In fact, we don't have a valid function set. The user has just typed a 'yes' or 'no' without a previous question from the chatbot
      await chatbotManager.answerToUser("", false).then(() => {});
      return;
    }

    let positive;
    let negative;
    if (strings.currentLanguage === "en") {
      positive = ["yes", "okay", "fine", "ok", "yeah", "yep", "sure"];
      negative = ["no", "nope"];
    } else if (strings.currentLanguage === "it") {
      positive = [
        "sì",
        "ok",
        "okay",
        "va bene",
        "si",
        "certo",
        "si certo",
        "si ok",
        "si va bene",
        "sisi",
        "va benissimo",
      ];
      negative = ["no", "nope"];
    }

    let isPositiveAnswer = null; // null means that it was not possible to determine the answer

    // remove all the non-alphabetical characters from the answer
    let cleanUtterance = nlpAnswer.utterance.replace(/[^a-zA-Z ]/g, "");

    // the user utterance is split into tokens of words and if any of those tokens is in the positive or
    // negative arrays, the answer is considered positive or negative, respectively.
    // so, even if the user doesn't answer exactly with one of the words in the arrays, the chatbot will
    // still be able to understand if the answer is positive or negative.
    if (cleanUtterance.split(" ").some((word) => positive.includes(word))) {
      isPositiveAnswer = true;
    } else if (
      cleanUtterance.split(" ").some((word) => negative.includes(word))
    ) {
      isPositiveAnswer = false;
    }

    if (isPositiveAnswer) {
      // If the answer is positive, execute the action saved in the ContextManager.continuePreviousIntent by the previous intent.
      // in this way, it's not necessary anymore to execute from scratch the previous intent, but just the
      // second action will be executed. In this way, we can avoid launching the same intent twice, hard-coding an
      // utterance to be sent to the chatbot.
      await contextManager.continuePreviousIntent();
    } else if (isPositiveAnswer === false) {
      chatbotManager.printChatbotAnswer("Okay!");
    } else {
      await chatbotManager.answerToUser("", false).then(() => {});
    }
  }
}

module.exports = YesNoIntent;
