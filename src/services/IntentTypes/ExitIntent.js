const Intent = require("./Intent");
const strings = require("../../data/strings");

/**
 * This intent allows the user to exit the chatbot. It has no real utility, since
 * the chatbot can stay active anyway, but it's useful to correctly answer to the
 * user if needed.
 */

const utterances = {
  en: ["close program", "bye", "exit program"],
  it: ["chiudi programma", "ciao", "esci", "spegniti"],
};

const answers = {
  en: ["Bye! Btw, I'll stay here, in case you need me again"],
  it: ["Ciao! Rimarrò qui, in caso tu abbia bisogno di me"],
};

class ExitIntent extends Intent {
  constructor(nlpManager) {
    super(
      strings.exitIntent,
      utterances[strings.currentLanguage],
      answers[strings.currentLanguage],
      nlpManager
    );
  }

  intentAction() {}
}

module.exports = ExitIntent;
