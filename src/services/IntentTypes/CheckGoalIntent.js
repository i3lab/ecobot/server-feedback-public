const Intent = require("./Intent");
const strings = require("../../data/strings");
const { loadConsumptionData } = require("../../utils/makeAPICall");
const ContextManager = require("../ContextManager");

/**
 * This intent allows the user to check the status of a set goal.
 */

const utterances = {
  en: [
    "what is the goal status for @appliance?",
    "goal status for @appliance",
    "status for the @appliance goal",
    "check goal for @appliance",
    "how many days I have before the goal for @appliance ends?",
  ],
  it: [
    "qual è lo stato del goal per @appliance?",
    "qual è lo stato dell'obiettivo per @appliance?",
    "stato del goal per @appliance",
    "stato dell'obiettivo per @appliance",
    "controlla il goal per @appliance",
    "controlla l'obiettivo per @appliance",
    "quanti giorni mancano prima che il goal per @appliance finisca?",
    "quanti giorni mancano prima che l'obiettivo per @appliance finisca?",
    "giorni rimanenti per il goal per @appliance",
    "giorni rimanenti per l'obiettivo per @appliance",
    "tra quanto finisce il goal per @appliance",
    "tra quanto finisce l'obiettivo per @appliance",
    "quando finisce il goal per @appliance",
    "quando finisce l'obiettivo per @appliance",
    "sto riuscendo a raggiungere l'obiettivo per @appliance?",
    "sto riuscendo a raggiungere il goal per @appliance?",
    "a quanto ammonta il goal per @appliance",
    "di quanto è il goal per @appliance",
    "a quanto è il goal per @appliance",
    "a quanto avevo messo il goal per @appliance",
    "a quanto avevo impostato il goal per @appliance",
    "a quanto avevo settato il goal per @appliance",
    "di quanto era il goal per @appliance",
    "a quanto avevo messo l'obiettivo per @appliance",
    "a quanto avevo impostato l'obiettivo per @appliance",
    "a quanto avevo settato l'obiettivo per @appliance",
    "di quanto era l'obiettivo per @appliance",
    "quant'è il goal per @appliance",
    "qual è il goal per @appliance",
    "qual è l'obiettivo per @appliance",
    "qual era il goal per @appliance",
    "qual era l'obiettivo per @appliance",
    "qual era il mio obiettivo per @appliance",
    "a quanto è impostato il goal per @appliance",
    "a quanto è impostato l'obiettivo per @appliance",
    "a quanto è settato il goal per @appliance",
    "a quanto è settato l'obiettivo per @appliance",
    "a quanto è messo il goal per @appliance",
    "a quanto è messo l'obiettivo per @appliance",
    "di quanto è l'obiettivo per @appliance",
    "di quanto era l'obiettivo per @appliance",
  ],
};

class CheckGoalIntent extends Intent {
  constructor(nlpManager) {
    super(
      strings.checkGoalIntent,
      utterances[strings.currentLanguage],
      [],
      nlpManager
    );
  }

  async intentAction(
    nlpAnswer,
    dialogManager,
    contextManager,
    intents,
    chatbotManager
  ) {
    // extract from the nlpAnswer the appliance name
    let currentAppliance;
    let currentApplianceText;
    for (const entity of nlpAnswer.entities) {
      if (entity.entity === strings.applianceEntity) {
        currentAppliance = entity.option;
        currentApplianceText = entity.sourceText;
      }
    }

    // if the appliance name was specified, we can continue. Otherwise, the slotFilling will ask the user to specify the appliance name, and we can close the intentAction
    if (!currentAppliance) {
      return;
    }

    // we can assume that the db is already filled with the list of all appliance in the appliances table
    const previousConsumptionsData = await loadConsumptionData(
      currentAppliance
    );

    // check if a goal is already set for the appliance
    if (!previousConsumptionsData.data.goal) {
      let generatedAnswer;
      if (strings.currentLanguage === "en") {
        generatedAnswer =
          "You still don't have a goal set for " +
          currentApplianceText +
          ". do you want to set a goal for it now? ";
      } else if (strings.currentLanguage === "it") {
        generatedAnswer =
          "Non hai ancora impostato un goal per " +
          currentApplianceText +
          ". Vuoi impostarne uno adesso? ";
      }
      // for that appliance, there is no goal set.
      chatbotManager.printChatbotAnswer(generatedAnswer);

      // put in the context the set goal intent, in order to set a goal if the users answers yes in the yesNoIntent
      ContextManager.prototype.continuePreviousIntent = async function () {
        let generatedAnswer;
        if (strings.currentLanguage === "en") {
          generatedAnswer = "set a goal for " + currentApplianceText;
        } else if (strings.currentLanguage === "it") {
          generatedAnswer = "imposta un obiettivo per " + currentApplianceText;
        }
        await chatbotManager
          .answerToUser(generatedAnswer, false)
          .then(() => {});
      };

      // set the continuePreviousIntent action as valid (for 1 intent)
      contextManager.continuePreviousIntentTokens = 2;
    } else {
      // there is a goal set. We first have to understand if the user is being able to reach the goal, based on the current consumption, the planned goal and the time left to reach the goal
      const currentConsumption =
        previousConsumptionsData.data.currentConsumptions || 0; // if there is no current consumption, we assume it is 0
      const goal = previousConsumptionsData.data.goal;
      const daysLeft = Math.round(
        (new Date(previousConsumptionsData.data.goalPeriodEnd) - Date.now()) /
          (1000 * 3600 * 24)
      ); // number of missing days from today to the goal period end

      const maxDailyConsumption = (goal / 30).toFixed(2); // the goal is always set for 30 days. If in total I can consume X, in a day I can consume X/30. Rounded to two digits
      let userDailyConsumption;
      if (currentConsumption === 0) {
        userDailyConsumption = 0;
      } else {
        userDailyConsumption = (currentConsumption / (31 - daysLeft)).toFixed(
          2
        ); // if I have already consumed Y, and there are still daysLeft, it means that, in a day, I consumed Y/(30-daysLeft). Rounded to two digits
      }

      if (maxDailyConsumption >= userDailyConsumption) {
        let generatedAnswer;
        if (strings.currentLanguage === "en") {
          generatedAnswer =
            "You have set a goal of " +
            goal +
            " kWh per month for " +
            currentApplianceText +
            ". You are being able to reach this goal: " +
            "You are consuming " +
            userDailyConsumption +
            " kWh per day, while the goal is " +
            maxDailyConsumption +
            " kWh per day. You still have " +
            daysLeft +
            " days until the end of the goal.";
        } else if (strings.currentLanguage === "it") {
          generatedAnswer =
            "Hai impostato un obiettivo di " +
            goal +
            " kWh al mese per " +
            currentApplianceText +
            ". Stai riuscendo a raggiungere questo obiettivo: " +
            "infatti, stai consumando " +
            userDailyConsumption +
            " kWh al giorno, mentre l'obiettivo è " +
            maxDailyConsumption +
            " kWh al giorno. Hai ancora " +
            daysLeft +
            " giorni(o) fino alla fine dell'obiettivo.";
        }
        // the user is being able to reach the goal
        chatbotManager.printChatbotAnswer(generatedAnswer);
      } else {
        let generatedAnswer;
        if (strings.currentLanguage === "en") {
          generatedAnswer =
            "You have set a goal of " +
            goal +
            " kWh per month for " +
            currentApplianceText +
            " You are consuming too much! At this pace, you will not reach the goal you have planned: " +
            "in fact,you are consuming " +
            userDailyConsumption +
            " kWh per day, while the goal is " +
            maxDailyConsumption +
            " kWh per day. You still have " +
            daysLeft +
            " days until the end of the goal. Do you want to receive a suggestion to understand how to improve your " +
            currentApplianceText +
            " consumption?";
        } else if (strings.currentLanguage === "it") {
          generatedAnswer =
            "Hai impostato un obiettivo di " +
            goal +
            " kWh al mese per " +
            currentApplianceText +
            ". Stai consumando troppo! A questo ritmo, non riuscirai a raggiungere l'obiettivo che hai pianificato: " +
            "infatti, stai consumando " +
            userDailyConsumption +
            " kWh al giorno, mentre l'obiettivo è " +
            maxDailyConsumption +
            " kWh al giorno. Hai ancora " +
            daysLeft +
            " giorni(o) fino alla fine dell'obiettivo. Vuoi ricevere un suggerimento per capire come migliorare il tuo consumo di " +
            currentApplianceText +
            "?";
        }
        // the user is not being able to reach the goal
        chatbotManager.printChatbotAnswer(generatedAnswer);
        // put in the context the suggestion intent, in order to provide an appliance suggestion if the users answers yes in the yesNoIntent
        ContextManager.prototype.continuePreviousIntent = async function () {
          let generatedAnswer;
          if (strings.currentLanguage === "en") {
            generatedAnswer =
              "give me a suggestion for " + currentApplianceText;
          } else if (strings.currentLanguage === "it") {
            generatedAnswer =
              "dammi un suggerimento per " + currentApplianceText;
          }
          await chatbotManager
            .answerToUser(generatedAnswer, false)
            .then(() => {});
        };

        // set the continuePreviousIntent action as valid (for 1 intent)
        contextManager.continuePreviousIntentTokens = 2;
      }
    }
  }
}

module.exports = CheckGoalIntent;
