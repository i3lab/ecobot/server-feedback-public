/**
 * Curiosity table is used to provide curiosities to the user for a given appliance
 */
module.exports = (sequelize, Sequelize) => {
  // noinspection JSUnresolvedVariable
  return sequelize.define("curiosity", {
    applianceName: {
      // the appliance the curiosity row is referring to. If null, the curiosity is general and not referring to a specific appliance
      type: Sequelize.STRING,
    },
    content: {
      // the curiosity the user will receive
      type: Sequelize.STRING,
    },
    type: {
      // can be useful to filter the curiosities for an appliance based on a parameter (eg: ECO_MODE curiosities, or TURN_OFF curiosities)
      type: Sequelize.STRING,
    },
  });
};
