const db = require("../models");
const Suggestion = db.suggestion;

/**
 * Create a new suggestion in the db, with related information
 */
exports.create = (req, res) => {
  const suggestion = {
    applianceName: req.body.applianceName,
    content: req.body.content,
    type: req.body.type,
  };

  Suggestion.create(suggestion)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Suggestion.",
      });
    });
};

/**
 * Load all suggestions for an appliance from the db
 */
exports.findAllForAppliance = (req, res) => {
  Suggestion.findAll({
    where: {
      applianceName: req.params.applianceName,
    },
  })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while retrieving suggestions for " +
            req.body.applianceName,
      });
    });
};

/**
 * Load all general suggestions
 */
exports.findAllGeneral = (req, res) => {
  Suggestion.findAll({
    where: {
      applianceName: null, // if appliance name is not specified, it means it is a general suggestion
    },
  })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while retrieving general suggestions",
      });
    });
};
