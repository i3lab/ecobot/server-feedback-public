const db = require("../models");
const User = db.user;

/**
 * Create a new user in the dd, with the related parameters (name, surname and region)
 */
exports.create = (req, res) => {
  const user = {
    name: req.body.name,
    surname: req.body.surname,
    region: req.body.region,
  };

  User.create(user)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the User.",
      });
    });
};

/**
 * Load a user by id from the database.
 * NOTE: The user ID will always be '1', since currently just one profile per time is supported.
 * But this is left open for an eventual future update
 */
exports.findOne = (req, res) => {
  const id = req.params.id;

  User.findByPk(id)
    .then((data) => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({
          message: `Cannot find User with id=${id}.`,
        });
      }
    })
    .catch(() => {
      res.status(500).send({
        message: "Error retrieving User with id=" + id,
      });
    });
};

/**
 * Update a user by id in the database. Useful, for example, to update the region the user is living into.
 */
exports.update = (req, res) => {
  const id = req.params.id;

  User.update(req.body, {
    where: { id: id },
  })
    .then((num) => {
      if (num === 1) {
        res.send({
          message: "User was updated successfully.",
        });
      } else {
        res.send({
          message: `Cannot update User with id=${id}. Maybe User was not found or req.body is empty!`,
        });
      }
    })
    .catch(() => {
      res.status(500).send({
        message: "Error updating User with id=" + id,
      });
    });
};
