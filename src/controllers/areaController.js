const db = require("../models");
const Area = db.area;

/**
 * Create a new area in the db, with related information (name, average consumptions)
 */
exports.create = (req, res) => {
  const area = {
    areaName: req.body.areaName,
    averageConsumptions: req.body.averageConsumptions,
  };

  Area.create(area)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the Area.",
      });
    });
};

/**
 * Load an area data by areaName from the database.
 */
exports.findOne = (req, res) => {
  const areaName = req.params.areaName;
  Area.findByPk(areaName)
    .then((data) => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({
          message: `Cannot find Area with name=${areaName}.`,
        });
      }
    })
    .catch(() => {
      res.status(500).send({
        message: "Error retrieving Area with name=" + areaName,
      });
    });
};
