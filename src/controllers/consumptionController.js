const db = require("../models");
const Consumption = db.consumption;

/**
 * Create a new consumption in the db, with related information (appliance, current consumptions, [goal])
 */
exports.create = (req, res) => {
  const consumption = {
    applianceName: req.body.applianceName,
    currentConsumptions: req.body.currentConsumptions,
    averageGlobalConsumptions: req.body.averageGlobalConsumptions,
    averageUserConsumptions: req.body.averageUserConsumptions,
    goal: req.body.goal,
    goalPeriodEnd: req.body.goalPeriodEnd,
  };

  Consumption.create(consumption)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Consumption.",
      });
    });
};

/**
 * Update a consumption by applianceName in the database.
 */
exports.update = (req, res) => {
  const applianceName = req.params.applianceName;

  Consumption.update(req.body, {
    where: { applianceName: applianceName },
  })
    .then((num) => {
      if (num === 1) {
        res.send({
          message: "Consumption was updated successfully.",
        });
      } else {
        res.send({
          message: `Cannot update Consumption with applianceName=${applianceName}. Maybe Consumption was not found or req.body is empty!`,
        });
      }
    })
    .catch(() => {
      res.status(500).send({
        message:
          "Error updating Consumption with applianceName=" + applianceName,
      });
    });
};

/**
 * Load all consumptions from the db
 */
exports.findAll = (req, res) => {
  Consumption.findAll()
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving consumptions.",
      });
    });
};

/**
 * Load a consumption data by applianceName from the database.
 */
exports.findOne = (req, res) => {
  const applianceName = req.params.applianceName;
  Consumption.findByPk(applianceName)
    .then((data) => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({
          message: `Cannot find Consumption with name=${applianceName}.`,
        });
      }
    })
    .catch(() => {
      res.status(500).send({
        message: "Error retrieving Consumption with name=" + applianceName,
      });
    });
};
