/**
 * Create a new notification in the RAM, with related information (type, appliance, priority, ID)
 */
const {
  getCurrentNotificationManager,
} = require("../utils/getCurrentManagers");
exports.create = (req) => {
  const notificationManager = getCurrentNotificationManager();
  notificationManager.addNotification(
    req.body.type,
    req.body.appliance,
    req.body.priority,
    req.body.message
  );
};

/**
 * Fetch the total number of notifications currently active (to show the message 'X new notifications' in the frontend).
 * Get a notification API is not required: the notification will be loaded
 * directly in the ReadNotificationIntent when the user requests it.
 */
exports.getNumber = (req, res) => {
  const notificationManager = getCurrentNotificationManager();
  res.send(notificationManager.notificationsNumber().toString());
};
