const strings = Object.freeze({
  // current language: [en, it]. Use this to change chatbot language. It will change utterances, answers, language recognition, entities, etc.
  currentLanguage: "it",

  // debug: activate to see the useful logs.
  printLogs: false,

  // entities
  applianceEntity: "appliance",
  regionEntity: "region",
  numberEntity: "number",

  // appliance names: the variable names must be the id ALL lowercase and without spaces + Appliance
  televisionAppliance: {
    id: "Television",
    en: ["television", "tv"],
    it: ["televisione", "tv", "televisore"],
  },
  dishwasherAppliance: {
    id: "Dishwasher",
    en: ["dishwasher", "dish washer"],
    it: ["lavastoviglie"],
  },
  washingmachineAppliance: {
    id: "Washing Machine",
    en: ["washing machine", "washing-machine"],
    it: ["lavatrice"],
  },
  driermachineAppliance: {
    id: "Drier Machine",
    en: ["drier machine", "drier-machine"],
    it: ["asciugatrice"],
  },
  fridgeAppliance: {
    id: "Fridge",
    en: ["fridge", "freezer"],
    it: ["frigorifero", "frigo", "freezer"],
  },
  airconditionerAppliance: {
    id: "Air Conditioner",
    en: [
      "air conditioner",
      "air conditioning",
      "air conditioning system",
      "AC",
    ],
    it: ["climatizzatore", "aria condizionata", "AC", "condizionatore", "l'aria condizionata"],
  },
  microwaveAppliance: {
    id: "Microwave",
    en: ["microwave", "microwave oven"],
    it: ["microonde"],
  },
  electricovenAppliance: {
    id: "Electric Oven",
    en: ["electric oven", "oven"],
    it: ["forno elettrico", "forno"],
  },
  inductionhobAppliance: {
    id: "Induction Hob",
    en: ["induction hob", "induction", "induction hobs"],
    it: ["fornelli a induzione", "fornelli"],
  },
  boilerAppliance: {
    id: "Boiler",
    en: ["boiler"],
    it: ["caldaia"],
  },
  entirehouseAppliance: {
    id: "Entire House",
    en: ["entire house", "house", "home"],
    it: [
      "intera casa",
      "casa",
      "tutti",
      "tutti i dispositivi",
      "tutti gli elettrodomestici",
    ],
  },

  // Remember ☝: when adding new appliances, the variable names MUST be the id ALL lowercase and without spaces + Appliance. For example: if the ID is Washing Machine, the variable name must be washingmachineAppliance.

  // region names
  abruzzoRegion: "Abruzzo",
  basilicataRegion: "Basilicata",
  calabriaRegion: "Calabria",
  campaniaRegion: "Campania",
  emiliaRomagnaRegion: "Emilia Romagna",
  friuliVeneziaGiuliaRegion: "Friuli Venezia Giulia",
  lazioRegion: "Lazio",
  liguriaRegion: "Liguria",
  lombardiaRegion: "Lombardia",
  marcheRegion: "Marche",
  moliseRegion: "Molise",
  piemonteRegion: "Piemonte",
  pugliaRegion: "Puglia",
  sardegnaRegion: "Sardegna",
  siciliaRegion: "Sicilia",
  toscanaRegion: "Toscana",
  trentinoAltoAdigeRegion: "Trentino Alto Adige",
  umbriaRegion: "Umbria",
  valDAostaRegion: "Val d'Aosta",
  venetoRegion: "Veneto",
  trentoRegion: "Trento",
  bolzanoRegion: "Bolzano",

  // intents
  areaIntent: "areaChart",
  checkGoalIntent: "goal.check",
  checkConsumptionIntent: "checkConsumption",
  curiosityIntent: "curiosity",
  decreaseGoalIntent: "goal.decrease",
  editGoalIntent: "goal.edit",
  exitIntent: "exit",
  againIntent: "again",
  increaseGoalIntent: "goal.increase",
  noneIntent: "None",
  removeGoalIntent: "goal.remove",
  setGoalIntent: "goal.set",
  specifyApplianceIntent: "specifyAppliance",
  specifyRegionIntent: "specifyRegion",
  suggestionIntent: "suggestion",
  yesNoIntent: "yesNo",
  readNotificationIntent: "readNotification",
  helpIntent: "helpIntent",
  listAppliancesIntent: "listAppliancesIntent",
  thanksIntent: "thanksIntent",
  costIntent: "costIntent",
  mostUsedApplianceIntent: "appliance.mostUsed",
  manageDeviceIntent: "appliance.manage",
  compareWithAverageIntent: "compareWithAverage",

  // slots questions
  whichApplianceSlotQuestion: {
    en: "For which appliance?",
    it: "Per quale dispositivo? Se vuoi, puoi anche dirmi 'tutti i dispositivi'.",
  },

  // fixed languages. Do not change this parameter, to change the chatbot language use currentLanguage parameter.
  englishLanguage: "en",

  // suggestions and curiosities types
  generalType: "general",
  ecoModeType: "ecoMode",
});

module.exports = strings;
